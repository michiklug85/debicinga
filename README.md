# IT System Monitoring with Icinga2

This repository is a project work for the FH Joanneum University of applied science in Austria.
The german documentation of how this installation can be used afterwards is included in this repository.

# Installation

A debian compatible Linux distribution is needed to use this repo for installing Icinga2 with Icingaweb2 and Icinga Director.

````bash
cd
mkdir debicinga
cd debicinga
git clone https://gitlab.com/michiklug85/debicinga.git
vim ~/.ansible.cfg
````
please add following settings:
````bash
[defaults]
roles_path=/etc/ansible/roles:~/debicinga/icinga2-ansible
````
than add the correct IP address to your inventory and start the playbook run:
````bash
vim inventory.ini #set correct IP address of the monitoring server
#than start the playbook
ansible-playbook site.yml -i inventory.ini
````

after finishing the playbook copy the WebUI token from the output and go to <http://icinga2ip/icingaweb2/setup>.

enter your token and start the setup.

On the modules page at least select the following modules 
  - monitoring 
  - director
  - ipl
  - reactbundle
  - incubator 

On the requirements screen just click next.

Authentication page keep database and click next.

enter 'icingaweb2' as database name username and password, validate the configuration and click next

keep the name for the Authentication Backend and just click next

enter a username and psssword for your administrative user and clock next

on the application configuration just click next

keep clicking next until your reach Monitoring IDO Resource

there as databasename user and password insert 'icinga' and click next

for the command transport use api with host 127.0.0.1 and user/password 'icingaweb2'

than click next once more and finally finished

###icingaweb2 setup finished! Now configure the director

You can now take a look at the dashboard to see the first few standard checks running on the server which hosts icinga2. 
We will now proceed to setup icinga director to be able to dynamically configure checks and hosts.

go to Configuration > Application > Resources > Create New Resource

Name 'director_db' | dbname, user and password: 'director' | charset utf8

Click on Icinga Director and select the newly created resource than click create schema, dont worry: this will take some 
time. Aftterwards in the API config for the kickstart in endpoint name enter the hostname of your icinga2 server. Host is
127.0.0.1 and user and password are 'icingaweb2'

Afterwards click on Icinga Infrastructure > Kickstart Wizard > Run Import

Finally click on Activity Log and deploy xxx changes, you are now set to use icinga2 and the director.